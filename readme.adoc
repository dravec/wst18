= Hello World

== Repository Clonen

```bash
git clone https://gitlab.com/stangenberg/wst18.git
cd wsd18
```

== Mit Docker

1. Get/Test Docker Container

```bash
docker run -e https_proxy -e http_proxy -v "$(pwd)":/app -w /app --user $(id -u):$(id -g) stangenberg/nodejs:latest serverless --version
```

2. Erstelle neues Projekt

```bash
docker run -e https_proxy -e http_proxy -v "$(pwd)":/app -w /app --user $(id -u):$(id -g) stangenberg/nodejs:latest serverless create --template aws-nodejs
```

Weitere Informationen unter: https://serverless.com/framework/docs/providers/aws/cli-reference/create/

3. Aktualisiere Service Name

- Öffne serverless.yml
- Ersetzte `service: aws-nodejs` mit `service: wsd18-aws-nodejs-<Deine Nummer>`

4. Aktualisiere AWS Credentials

- Kopiere .aws/credentials.template nach .aws/credentials `cp .aws/credentials.template .aws/credentials`
- Editiere die .aws/credentials und trage aws_access_key_id und aws_secret_access_key ein.

5. Deploy Service

```bash
docker run -e https_proxy -e http_proxy -v "$(pwd)":/app -w /app --user $(id -u):$(id -g) stangenberg/nodejs:latest serverless deploy -v
```

Weitere Informationen unter: https://serverless.com/framework/docs/providers/aws/guide/deploying/

6. Service aufrufen

```bash
docker run -e https_proxy -e http_proxy -v "$(pwd)":/app -w /app --user $(id -u):$(id -g) stangenberg/nodejs:latest serverless invoke -f hello
```

Weitere Informationen unter: https://serverless.com/framework/docs/providers/aws/cli-reference/invoke/

7. Web console

https://console.aws.amazon.com/console/home?region=us-east-1


== Mit installiertem Node.js

1. Installiere Serverless

```bash
npm install -g serverless
```

2. Erstelle neues Projekt

```bash
serverless create --template aws-nodejs
```

Weitere Informationen unter: https://serverless.com/framework/docs/providers/aws/cli-reference/create/

3. Aktualisiere Service Name

- Öffne serverless.yml
- Ersetzte `service: aws-nodejs` mit `service: wsd18-aws-nodejs-<Deine Nummer>`

4. Aktualisiere AWS Credentials

- Kopiere .aws/credentials.template nach .aws/credentials `cp .aws/credentials.template .aws/credentials`
- Editiere die .aws/credentials und trage aws_access_key_id und aws_secret_access_key ein.

5. Deploy Service

```bash
serverless deploy -v
```

Weitere Informationen unter: https://serverless.com/framework/docs/providers/aws/guide/deploying/


6. Service aufrufen

```bash
serverless invoke -f hello
```

Weitere Informationen unter: https://serverless.com/framework/docs/providers/aws/cli-reference/invoke/

7. Web console

https://console.aws.amazon.com/console/home?region=us-east-1
